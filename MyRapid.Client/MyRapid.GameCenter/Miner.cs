/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
using MyRapid.Base.Page;
using MyRapid.Code;
using MyRapid.SysEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyRapid.GameCenter
{
    public partial class Miner : ChildPage
    {
        public Miner()
        {
            InitializeComponent();
        }
        List<GridBlock> cells = new List<GridBlock>();

        private void Sudoku_Click(object sender, EventArgs e)
        {
            cells.Clear();
            ShowGrid.BackgroundColor = Color.Black;
            ShowGrid.BlockOffset = 2;
            ShowGrid.BorderColor = Color.White;
            ShowGrid.BorderSize = 1;
            ShowGrid.BlockFont = new Font("微软雅黑", 28);
            ShowGrid.BlockColor = Color.White;
            ShowGrid.GridColumnCount = 9;
            ShowGrid.GridRowCount = 9;
            ShowGrid.BlockWidth = (pictureBox1.Width - ShowGrid.BlockOffset) / ShowGrid.GridColumnCount - ShowGrid.BlockOffset;
            ShowGrid.BlockHeight = (pictureBox1.Height - ShowGrid.BlockOffset) / ShowGrid.GridRowCount - ShowGrid.BlockOffset;
            ShowGrid.InitializeGrid(pictureBox1);
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    GridBlock cell = new GridBlock();
                    cell.BlockRowIndex = i;
                    cell.BlockColumnIndex = j;
                    cell.BlockText = "";// (DateTime.Now.Millisecond % 9 + 1).ToStringEx();    // (i * 10 + j).ToStringEx(); // //
                    cell.BlockIndex = i * 9 + j;
                    cell.BlockColor = Color.Gray;
                    cell.BorderColor = Color.White;
                    cells.Add(cell);
                    ShowGrid.PaintBlock(cell);

                }
            }
            booms.Clear();
            while (booms.Count < 10)
            {
                Random r = new Random();
                int index = r.Next(0, 81);
                if (booms.Find(b => b.BlockIndex.Equals(index)) ==null)
                {
                    cells[index].BlockText = "@";
                    cells[index].BlockColor = Color.Red;
                    booms.Add(cells[index]);


                    //ShowGrid.PaintBlock(cells[index]);
                }
            }

            //foreach (GridBlock cell in cells)
            //{
            //    CheckResult(cell, true);
            //}


        }

        List<GridBlock> booms = new List<GridBlock>();

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            GridBlock cell = cells.Find(c => c.BlockLocation.X < e.X && c.BlockLocation.Y < e.Y
         && c.BlockLocation.X + c.BlockSize.Width > e.X && c.BlockLocation.Y + c.BlockSize.Height > e.Y);
            if (cell ==null) return;
            if (e.Button.Equals(MouseButtons.Left))
            {
                GridBlock boom = booms.Find(b => b.BlockRowIndex.Equals(cell.BlockRowIndex) && b.BlockColumnIndex.Equals(cell.BlockColumnIndex));
                if (boom != null)
                {
                    foreach (GridBlock gb in booms)
                    {
                        ShowGrid.PaintBlock(gb);
                    }
                    return;
                }
                clicked.Clear();
                ClickZero(cell);
            }
            else
            {
                if (cell.BlockText.Equals("@"))
                {
                    cell.BlockColor = Color.Green;
                    ShowGrid.PaintBlock(cell);
                }
                else
                {
                    cell.BlockText = GetCount(cell).ToStringEx();
                    cell.BlockColor = Color.Red;
                    ShowGrid.PaintBlock(cell);
                }
            }
        }

        private List<GridBlock> clicked = new List<GridBlock>();

        private void ClickZero(GridBlock cell)
        {
            clicked.Add(cell);
            cell.BlockText = GetCount(cell).ToStringEx();
            cell.BlockColor = Color.White;
            ShowGrid.PaintBlock(cell);
            if (cell.BlockText.Equals("0"))
            {
                foreach (GridBlock gb in cells.Where(c => c.BlockRowIndex >= cell.BlockRowIndex - 1
               && c.BlockRowIndex <= cell.BlockRowIndex + 1
               && c.BlockColumnIndex >= cell.BlockColumnIndex - 1
               && c.BlockColumnIndex <= cell.BlockColumnIndex + 1
               && c.BlockIndex != cell.BlockIndex))
                {
                    if (clicked.Find(c => c.BlockIndex.Equals(gb.BlockIndex)) ==null)
                        ClickZero(gb);
                }
            }
        }


        private int GetCount(GridBlock cell)
        {
            int cnt = booms.Where(c => c.BlockRowIndex >= cell.BlockRowIndex - 1
               && c.BlockRowIndex <= cell.BlockRowIndex + 1
               && c.BlockColumnIndex >= cell.BlockColumnIndex - 1
               && c.BlockColumnIndex <= cell.BlockColumnIndex + 1).Count();
            return cnt;

        }

    }
}