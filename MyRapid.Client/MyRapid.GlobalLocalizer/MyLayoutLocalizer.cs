/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
using DevExpress.XtraLayout.Localization;
using MyRapid.Client.Service;
using MyRapid.Client.Service.MainService;
using MyRapid.SysEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
namespace MyRapid.GlobalLocalizer
{
    public class MyLayoutLocalizer : LayoutLocalizer
    {
        public override string GetLocalizedString(LayoutStringId ID)
        {
            return BaseLocalizer.GetLocalizedString<LayoutStringId>((int)ID, base.GetLocalizedString(ID), base.GetType().Name);
        }
    }
}