/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Configuration;
using System.Text;
using System.Threading.Tasks;
namespace MyRapid.Server.Extension
{
    public class CustomErrorBehaviorExtension : BehaviorExtensionElement
    {
        public override Type BehaviorType => typeof(CustomErrorBehavior);
        protected override object CreateBehavior()
        {
            return new CustomErrorBehavior();
        }
    }
}
 