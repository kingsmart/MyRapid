/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
namespace MyRapid.SystemManager
{
    partial class SystemLayout
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SystemLayout));
            this.layout1 = new MyRapid.Base.Edit.Layout(this.components);
            this.splitterControl2 = new DevExpress.XtraEditors.SplitterControl();
            this.gcb = new DevExpress.XtraEditors.GroupControl();
            this.gdb = new DevExpress.XtraGrid.GridControl();
            this.gvb = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.qry = new DevExpress.XtraEditors.GroupControl();
            this.cPage_Id = new DevExpress.XtraEditors.LookUpEdit();
            this.gcf = new DevExpress.XtraEditors.GroupControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.splitterControl5 = new DevExpress.XtraEditors.SplitterControl();
            ((System.ComponentModel.ISupportInitialize)(this.gcb)).BeginInit();
            this.gcb.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gdb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qry)).BeginInit();
            this.qry.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cPage_Id.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitterControl2
            // 
            this.splitterControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitterControl2.Location = new System.Drawing.Point(493, 31);
            this.splitterControl2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.splitterControl2.Name = "splitterControl2";
            this.splitterControl2.Size = new System.Drawing.Size(5, 498);
            this.splitterControl2.TabIndex = 6;
            this.splitterControl2.TabStop = false;
            // 
            // gcb
            // 
            this.gcb.Controls.Add(this.gdb);
            this.gcb.Dock = System.Windows.Forms.DockStyle.Right;
            this.gcb.Location = new System.Drawing.Point(498, 31);
            this.gcb.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gcb.Name = "gcb";
            this.gcb.Size = new System.Drawing.Size(500, 498);
            this.gcb.TabIndex = 7;
            this.gcb.Text = "信息";
            // 
            // gdb
            // 
            this.gdb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gdb.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gdb.Location = new System.Drawing.Point(2, 21);
            this.gdb.MainView = this.gvb;
            this.gdb.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gdb.Name = "gdb";
            this.gdb.Size = new System.Drawing.Size(496, 475);
            this.gdb.TabIndex = 0;
            this.gdb.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvb});
            // 
            // gvb
            // 
            this.gvb.GridControl = this.gdb;
            this.gvb.Name = "gvb";
            this.gvb.OptionsSelection.MultiSelect = true;
            this.gvb.OptionsView.ShowFooter = true;
            this.gvb.OptionsView.ShowGroupPanel = false;
            // 
            // qry
            // 
            this.qry.Controls.Add(this.cPage_Id);
            this.qry.Dock = System.Windows.Forms.DockStyle.Top;
            this.qry.Location = new System.Drawing.Point(0, 0);
            this.qry.Name = "qry";
            this.qry.Size = new System.Drawing.Size(493, 80);
            this.qry.TabIndex = 0;
            this.qry.Text = "布局页面";
            // 
            // cPage_Id
            // 
            this.cPage_Id.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cPage_Id.Location = new System.Drawing.Point(2, 21);
            this.cPage_Id.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cPage_Id.Name = "cPage_Id";
            this.cPage_Id.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cPage_Id.Properties.NullText = "";
            this.cPage_Id.Size = new System.Drawing.Size(489, 20);
            this.cPage_Id.TabIndex = 0;
            // 
            // gcf
            // 
            this.gcf.CustomHeaderButtons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraEditors.ButtonsPanelControl.GroupBoxButton("tf", ((System.Drawing.Image)(resources.GetObject("gcf.CustomHeaderButtons"))), false, true, ""),
            new DevExpress.XtraEditors.ButtonsPanelControl.GroupBoxButton("tbf", ((System.Drawing.Image)(resources.GetObject("gcf.CustomHeaderButtons1"))), false, true, ""),
            new DevExpress.XtraEditors.ButtonsPanelControl.GroupBoxButton("ttf", ((System.Drawing.Image)(resources.GetObject("gcf.CustomHeaderButtons2"))), false, true, ""),
            new DevExpress.XtraEditors.ButtonsPanelControl.GroupBoxButton("tlf", ((System.Drawing.Image)(resources.GetObject("gcf.CustomHeaderButtons3"))), false, true, ""),
            new DevExpress.XtraEditors.ButtonsPanelControl.GroupBoxButton("trf", ((System.Drawing.Image)(resources.GetObject("gcf.CustomHeaderButtons4"))), false, true, ""),
            new DevExpress.XtraEditors.ButtonsPanelControl.GroupBoxButton("tlrf", ((System.Drawing.Image)(resources.GetObject("gcf.CustomHeaderButtons5"))), false, true, ""),
            new DevExpress.XtraEditors.ButtonsPanelControl.GroupBoxButton("tllf", ((System.Drawing.Image)(resources.GetObject("gcf.CustomHeaderButtons6"))), false, true, ""),
            new DevExpress.XtraEditors.ButtonsPanelControl.GroupBoxButton("trrf", ((System.Drawing.Image)(resources.GetObject("gcf.CustomHeaderButtons7"))), false, true, ""),
            new DevExpress.XtraEditors.ButtonsPanelControl.GroupBoxButton("tblf", ((System.Drawing.Image)(resources.GetObject("gcf.CustomHeaderButtons8"))), false, true, ""),
            new DevExpress.XtraEditors.ButtonsPanelControl.GroupBoxButton("tbrf", ((System.Drawing.Image)(resources.GetObject("gcf.CustomHeaderButtons9"))), false, true, ""),
            new DevExpress.XtraEditors.ButtonsPanelControl.GroupBoxButton("tltf", ((System.Drawing.Image)(resources.GetObject("gcf.CustomHeaderButtons10"))), false, true, ""),
            new DevExpress.XtraEditors.ButtonsPanelControl.GroupBoxButton("tlfb", ((System.Drawing.Image)(resources.GetObject("gcf.CustomHeaderButtons11"))), false, true, ""),
            new DevExpress.XtraEditors.ButtonsPanelControl.GroupBoxButton("trfb", ((System.Drawing.Image)(resources.GetObject("gcf.CustomHeaderButtons12"))), false, true, ""),
            new DevExpress.XtraEditors.ButtonsPanelControl.GroupBoxButton("trtf", ((System.Drawing.Image)(resources.GetObject("gcf.CustomHeaderButtons13"))), false, true, ""),
            new DevExpress.XtraEditors.ButtonsPanelControl.GroupBoxButton("prv", ((System.Drawing.Image)(resources.GetObject("gcf.CustomHeaderButtons14"))), false, true, "")});
            this.gcf.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcf.Location = new System.Drawing.Point(0, 85);
            this.gcf.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gcf.Name = "gcf";
            this.gcf.Size = new System.Drawing.Size(493, 413);
            this.gcf.TabIndex = 10;
            this.gcf.CustomButtonClick += new DevExpress.XtraBars.Docking2010.BaseButtonEventHandler(this.gcf_CustomButtonClick);
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.gcf);
            this.panelControl1.Controls.Add(this.splitterControl5);
            this.panelControl1.Controls.Add(this.qry);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 31);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(493, 498);
            this.panelControl1.TabIndex = 1;
            // 
            // splitterControl5
            // 
            this.splitterControl5.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitterControl5.Location = new System.Drawing.Point(0, 80);
            this.splitterControl5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.splitterControl5.Name = "splitterControl5";
            this.splitterControl5.Size = new System.Drawing.Size(493, 5);
            this.splitterControl5.TabIndex = 24;
            this.splitterControl5.TabStop = false;
            // 
            // SystemLayout
            // 
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(998, 556);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.splitterControl2);
            this.Controls.Add(this.gcb);
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "SystemLayout";
            this.QueryControl = this.qry;
            this.Text = "SystemLayout";
            this.Shown += new System.EventHandler(this.SystemLayout_Shown);
            this.Controls.SetChildIndex(this.gcb, 0);
            this.Controls.SetChildIndex(this.splitterControl2, 0);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.gcb)).EndInit();
            this.gcb.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gdb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qry)).EndInit();
            this.qry.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cPage_Id.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();
        }
        #endregion
        private Base.Edit.Layout layout1;
        private DevExpress.XtraEditors.SplitterControl splitterControl2;
        private DevExpress.XtraEditors.GroupControl gcb;
        private DevExpress.XtraGrid.GridControl gdb;
        private DevExpress.XtraGrid.Views.Grid.GridView gvb;
        private DevExpress.XtraEditors.GroupControl qry;
        private DevExpress.XtraEditors.LookUpEdit cPage_Id;
        private DevExpress.XtraEditors.GroupControl gcf;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SplitterControl splitterControl5;
    }
}