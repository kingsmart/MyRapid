/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using System.IO;
using DevExpress.Spreadsheet;
using DevExpress.XtraSpreadsheet;

namespace MyRapid.Base.Designer
{
    public partial class SpreadsheetDesigner : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public SpreadsheetDesigner()
        {
            InitializeComponent();

        }
        //存储编辑后的Byte[]
        public byte[] ByteContent;
        public byte[] ByteGenerate;
        //存储源文件路径
        public string SourceFile { get; set; }
        public object DataSource { get; set; }
        public Dictionary<string, object> Parameters { get; set; }
        /// <summary>
        /// 传入文本 
        /// </summary>
        /// <param name="fileName">可以是文件路径 或 xml</param>
        public SpreadsheetDesigner(object dSet)
        {
            InitializeComponent();
            DataSource = dSet;
            spreadsheetControl.CreateNewDocument();
            spreadsheetControl.Document.MailMergeDataSource = dSet;
            spreadsheetControl.SaveDocument(rptName);
            SourceFile = rptName;
        }
        /// <summary>
        /// 传入文本 
        /// </summary>
        /// <param name="fileName">可以是文件路径 或 xml</param>
        public SpreadsheetDesigner(object dSet, Dictionary<string, object> pt)
        {
            InitializeComponent();
            Parameters = pt;
            DataSource = dSet;
            spreadsheetControl.CreateNewDocument();
            spreadsheetControl.Document.MailMergeDataSource = dSet;

            foreach (var p in pt)
            {
                spreadsheetControl.Document.MailMergeParameters.AddParameter(p.Key, p.Value);
            }
            spreadsheetControl.SaveDocument(rptName);
            SourceFile = rptName;

        }

        /// <summary>
        /// 传入文本 
        /// </summary>
        /// <param name="fileName">可以是文件路径</param>
        public SpreadsheetDesigner(string fileName, object dSet)
        {
            InitializeComponent();
            DataSource = dSet;
            try
            {
                this.DataSource = dSet;
                if (string.IsNullOrEmpty(fileName))
                {
                    return;
                }
                if (File.Exists(fileName))
                {
                    spreadsheetControl.LoadDocument(fileName);
                    SourceFile = fileName;
                }
                else
                {
                    spreadsheetControl.CreateNewDocument();
                    spreadsheetControl.Document.MailMergeDataSource = dSet;
                    spreadsheetControl.SaveDocument(rptName);
                    SourceFile = rptName;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// 传入字节
        /// </summary>
        /// <param name="bytes">文件字节</param>
        /// <param name="documentFormat">格式</param>
        public SpreadsheetDesigner(byte[] bytes, object dSet)
        {
            InitializeComponent();
            try
            {
                this.DataSource = dSet;
                if (bytes == null) return;
                spreadsheetControl.LoadDocument(bytes, DevExpress.Spreadsheet.DocumentFormat.Xlsx);
                spreadsheetControl.Document.MailMergeDataSource = dSet;
                ByteContent = bytes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 传入字节
        /// </summary>
        /// <param name="bytes">文件字节</param>
        /// <param name="documentFormat">格式</param>
        public SpreadsheetDesigner(byte[] bytes, object dSet, Dictionary<string, object> pt)
        {
            InitializeComponent();
            try
            {
                this.DataSource = dSet;
                ByteContent = bytes;
                Parameters = pt;
                if (bytes == null) return;
                spreadsheetControl.LoadDocument(bytes, DevExpress.Spreadsheet.DocumentFormat.Xlsx);
                spreadsheetControl.Document.MailMergeDataSource = dSet;
                foreach (var p in pt)
                {
                    spreadsheetControl.Document.MailMergeParameters.AddParameter(p.Key, p.Value);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void Spreadsheet_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (spreadsheetControl.ActiveWorksheet != null)
                {
                    MemoryStream ms = new MemoryStream();
                    IWorkbook wb = spreadsheetControl.Document;
                    IList<IWorkbook> lwb = wb.GenerateMailMergeDocuments();
                    if (lwb.Count > 0)
                    {
                        MemoryStream gms = new MemoryStream();
                        lwb[0].SaveDocument(gms, DocumentFormat.Xlsx);
                        ByteGenerate = gms.ToArray();
                    }
                    spreadsheetControl.SaveDocument(ms, DevExpress.Spreadsheet.DocumentFormat.Xlsx);
                    ByteContent = ms.ToArray();
                }
                if (File.Exists(rptName)) File.Delete(rptName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string rptName = Guid.NewGuid().ToString();

        private void SpreadsheetDesigner_Resize(object sender, EventArgs e)
        {
            iStatic.Caption = string.Format("{0}x{1}", spreadsheetControl.Width, spreadsheetControl.Height);
        }


    }
}