/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
namespace MyRapid.Server.Service
{
    [Serializable]
    [DataContract]
    public class MyParameter
    {
        public MyParameter()
        {

        }
        public MyParameter(string name, object value, int? type, string source)
        {
            this.Name = name;
            this.Value = value;
            this.Type = type;
            this.Source = source;
        }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public object Value { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public int? Type { get; set; }
    }
}