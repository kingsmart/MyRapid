/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
using MyRapid.Base.Page;
using MyRapid.Code;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyRapid.HardwareManager
{
    public partial class SerialDebug : ChildPage
    {
        public SerialDebug()
        {
            InitializeComponent();
            foreach (string port in SerialPort.GetPortNames())
            {
                txtName.Properties.Items.Add(port);
            }
            txtName.SelectedIndex = 0;
            serialPort.DataReceived += SerialPort_DataReceived;
        }

        private void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                if (chkReceive.Checked)
                {
                    byte[] bytes = new byte[serialPort.BytesToRead];
                    serialPort.Read(bytes, 0, serialPort.BytesToRead);
                    this.Invoke(new UpdateTextEventHandler(UpdateTextBox), new string[] { BitConverter.ToString(bytes) });
                    serialPort.DiscardInBuffer();
                    serialPort.DiscardOutBuffer();
                }
                else
                {
                    string rec = serialPort.ReadExisting();
                    txtReceive.Invoke(new UpdateTextEventHandler(UpdateTextBox), new string[] { rec });
                    serialPort.DiscardInBuffer();
                    serialPort.DiscardOutBuffer();
                }
            }
            catch (Exception ex)
            {
                SharedFunc.RaiseError(ex);
            }

        }

        delegate void UpdateTextEventHandler(string text);
        private void UpdateTextBox(string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                txtReceive.Text = "接收：" + text + "\t时间：" + DateTime.Now.ToString() + "\r\n" + txtReceive.Text;
            }
        }



        SerialPort serialPort = new SerialPort();
        private void btnOpen_Click(object sender, EventArgs e)
        {
            try
            {
                if (!serialPort.IsOpen)
                {
                    serialPort.BaudRate = txtBaud.Text.ToIntEx();
                    serialPort.PortName = txtName.Text;
                    serialPort.DataBits = txtData.Text.ToIntEx();
                    serialPort.StopBits = (StopBits)(txtStop.SelectedIndex + 1);
                    serialPort.Parity = (Parity)txtCheck.SelectedIndex;
                    serialPort.Open();
                    if (serialPort.IsOpen)
                    {
                        btnOpen.Text = "关闭串口";
                    }
                }
                else
                {
                    serialPort.Close();
                    if (!serialPort.IsOpen)
                    {
                        btnOpen.Text = "打开串口";
                    }
                }
            }
            catch (Exception ex)
            {
                SharedFunc.RaiseError(ex);
            }

        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            SendData();
        }

        private void SendData()
        {
            try
            {
                if(!txtSend .Properties .Items .Contains (txtSend.Text))
                {
                    txtSend.Properties.Items.Add(txtSend.Text);
                }
            }
            catch (Exception ex)
            {
                SharedFunc.RaiseError(ex);
            }
            try
            {
                if (chkSend.Checked)
                {
                    byte[] bytes = Encoding.Default.GetBytes(txtSend.Text);
                    serialPort.Write(bytes, 0, bytes.Length);
                    txtReceive.Text = "发送：" + BitConverter.ToString(bytes) + "\t时间：" + DateTime.Now.ToString() + "\r\n" + txtReceive.Text;
                }
                else
                {
                    serialPort.Write(txtSend.Text);
                    txtReceive.Text = "发送：" + txtSend.Text + "\t时间：" + DateTime.Now.ToString() + "\r\n" + txtReceive.Text;
                }
                if (chkClear.Checked)
                {
                    txtSend.Text = "";
                }
            }
            catch (Exception ex)
            {
                SharedFunc.RaiseError(ex);
            }
        }

        private void timerSend_Tick(object sender, EventArgs e)
        {

            if (chkAuto.Checked && !string.IsNullOrEmpty(txtSend.Text))
            {
                SendData();
            }
        }

        private void chkAuto_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkAuto.Checked)
                {
                    timerSend.Interval = txtTime.Text.ToIntEx() * 1000 + 1;
                    timerSend.Enabled = true;
                }
                else
                {
                    timerSend.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                SharedFunc.RaiseError(ex);
            }
        }
    }
}