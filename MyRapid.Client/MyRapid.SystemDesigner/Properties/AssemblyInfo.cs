/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("MyRapid.Base.Designer")]
[assembly: AssemblyDescription("MyRapid快速开发框架")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("若彼得软件科技 (18115503914)")]
[assembly: AssemblyProduct("MyRapid")]
[assembly: AssemblyCopyright("Copyright © 陈恩点 2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following string is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("c20b63ff-4512-4b80-90d8-b7e90d7a9328")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("18.1108.6886.380")]
[assembly: AssemblyFileVersion("18.1108.6886.380")]