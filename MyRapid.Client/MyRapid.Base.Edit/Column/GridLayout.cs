/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
namespace MyRapid.Base.Edit
{
    public partial class GridLayout : Component
    {
        public GridLayout()
        {
            InitializeComponent();
        }
        public GridLayout(IContainer container)
        {
            container.Add(this);
            InitializeComponent();
        }
    }
}