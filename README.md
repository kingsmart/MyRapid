
### MyRapid WinForm快速开发框架
一个插件式快速开发框架

- 为了兼容XP用的是.Net 4 Dev 也用的是16.2(32位)
- 放弃XP的朋友可以自行升级
- DevExpress 16.2请自行下载
- QQ交流群：285266980


### MyRapid 原理


- 整体框架主要面向的是表格，比如销售订单在系统里面会抽象为主表子表，在用户操作的时候也是直接填写两张表格
- 框架用的是ADO.NET，保存使用的是ADO的Update命令
- 控件是根据页面编辑中的配置自动生成出来的，所以可以根据权限来决定某个控件是否需要生成
- WCF服务端只有5个方法，登录、读取、保存、执行、上传、下载，业务逻辑都是通过客户端代码 和 Sql 实现的
- 表格的增删改查代码都存放在WorkSet里面，调用时用存储过程根据WorkSet的Id或Name取得增删改查代码
- 页面所有的public的方法都会反射到页面编辑的Tool里面，可以根据需要决定是否显示
- 弹窗也是自动生成出来的，根据WorkSet取得数据源，根据弹窗配置取得返回值
- 报表在框架的系统报表界面进行配置，然后到页面编辑-Tool-Print里面设置参数即可打印调用

### MyRapid 框架介绍


- 开发历程：作者是数据库相关软件开发从业人员，懒惰的，能交给电脑做的事情懒得自己做
- 开发目的：处理底层数据传输，减少工作量，提高开发效率
- 框架特点：数据库相关开发、易学易用、快速上手、提高效率、高度开放、多国语言、多种皮肤
- 框架功能：表单生成、图表生成
- 表单生成：自动生成页面，具备基础增删改查功能(多表)
- 数据库：支持SqlServer + 任意数据库(必须有一个SqlServer)
- WCF：全新的架构，安全性得到了很好的保证
- 日志系统：WCF消息拦截记录日志
- 树状目录：树状无限级目录
- 权限控制：权限控制到页面特定按钮以及特定字段
- 图标库：更大的图标库，支持扩容
- 数据验证：全局控件数据验证，保证数据正确性


![用户信息](https://images.gitee.com/uploads/images/2020/1023/090031_0e48393d_299806.png "用户信息.png")

![权限控制](https://images.gitee.com/uploads/images/2020/1023/090044_80989ac9_299806.png "权限控制.png")