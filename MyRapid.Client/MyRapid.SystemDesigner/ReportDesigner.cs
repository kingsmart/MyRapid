/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
using DevExpress.XtraReports.UI;
using MyRapid.Code;
using System;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace MyRapid.Base.Designer
{
    public partial class ReportDesigner : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public ReportDesigner()
        {
            InitializeComponent();
        }

        //存储编辑后的rtf
        public string Content;
        //存储编辑后的Byte[]
        public byte[] ByteContent;
        //存储源文件路径
        private string SourceFile;
        private object dSet;
        /// <summary>
        /// 传入文本 
        /// </summary>
        /// <param name="fileName">可以是文件路径 或 xml</param>
        public ReportDesigner(object dSet)
        {
            InitializeComponent();
            this.dSet = dSet;

        }

        /// <summary>
        /// 传入文本 
        /// </summary>
        /// <param name="fileName">可以是文件路径 或 xml</param>
        public ReportDesigner(string fileName, object dSet)
        {
            InitializeComponent();
            try
            {
                this.dSet = dSet;
                if (string.IsNullOrEmpty(fileName))
                {
                    return;
                }
                if (File.Exists(fileName))
                {
                    reportDesigner1.OpenReport(fileName);
                    SourceFile = fileName;
                }
                else
                {
                    XtraReport xtraReport = new XtraReport();
                    xtraReport.DataSource = dSet;
                    File.WriteAllText(rptName, fileName);
                    xtraReport.LoadLayoutFromXml(rptName);
                    reportDesigner1.OpenReport(xtraReport);
                    SourceFile = rptName;
                    Content = fileName;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// 传入字节
        /// </summary>
        /// <param name="bytes">文件字节</param>
        /// <param name="documentFormat">格式</param>
        public ReportDesigner(byte[] bytes, object dSet)
        {
            InitializeComponent();
            try
            {
                this.dSet = dSet;
                if (bytes == null) return;
                XtraReport xtraReport = new XtraReport();
                xtraReport.DataSource = dSet;
                ByteContent = bytes;
                xtraReport.LoadLayout(bytes.ToStream());
                reportDesigner1.OpenReport(xtraReport);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 传入字节
        /// </summary>
        /// <param name="bytes">文件字节</param>
        /// <param name="documentFormat">格式</param>
        public ReportDesigner(byte[] bytes, object dSet, DevExpress.XtraReports.Parameters.Parameter[] pt)
        {
            InitializeComponent();
            try
            {
                this.dSet = dSet;
                if (bytes == null) return;
                XtraReport xtraReport = new XtraReport();
                xtraReport.RequestParameters = false;
                ByteContent = bytes;
                xtraReport.LoadLayout(bytes.ToStream());
                xtraReport.DataSource = dSet;
                xtraReport.Parameters.Clear();
                xtraReport.Parameters.AddRange(pt);
                reportDesigner1.OpenReport(xtraReport);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string rptName = Guid.NewGuid().ToString();

        private void ReportDesigner_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (reportDesigner1.ActiveDesignPanel != null)
                {
                    MemoryStream ms = new MemoryStream();
                    reportDesigner1.ActiveDesignPanel.Report.SaveLayout(ms);
                    ByteContent = ms.ToArray();
                    MemoryStream xms = new MemoryStream();
                    reportDesigner1.ActiveDesignPanel.Report.SaveLayoutToXml(xms);
                    xms.Position = 0;
                    StreamReader reader = new StreamReader(xms);
                    Content = reader.ReadToEnd();
                }
                if (File.Exists(rptName)) File.Delete(rptName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void reportDesigner1_AnyDocumentActivated(object sender, DevExpress.XtraBars.Docking2010.Views.DocumentEventArgs e)
        {
            try
            {
                if (reportDesigner1.ActiveDesignPanel != null)
                {
                    reportDesigner1.ActiveDesignPanel.FileName = rptName;
                    reportDesigner1.ActiveDesignPanel.Report.DataSource = dSet;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}