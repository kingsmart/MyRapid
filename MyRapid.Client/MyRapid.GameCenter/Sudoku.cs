/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
using MyRapid.Base.Page;
using MyRapid.Code;
using MyRapid.SysEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyRapid.GameCenter
{
    public partial class Sudoku : ChildPage
    {
        public Sudoku()
        {
            InitializeComponent();
        }
        List<GridBlock> cells = new List<GridBlock>();

        private void Sudoku_Click(object sender, EventArgs e)
        {
            cells.Clear();
            ShowGrid.BackgroundColor = Color.Black;
            ShowGrid.BlockOffset = 2;
            ShowGrid.BorderColor = Color.White;
            ShowGrid.BorderSize = 1;
            ShowGrid.BlockFont = new Font("微软雅黑", 28);
            ShowGrid.BlockColor = Color.White;
            ShowGrid.GridColumnCount = 9;
            ShowGrid.GridRowCount = 9;
            ShowGrid.BlockWidth = (pictureBox1.Width - ShowGrid.BlockOffset) / ShowGrid.GridColumnCount - ShowGrid.BlockOffset;
            ShowGrid.BlockHeight = (pictureBox1.Height - ShowGrid.BlockOffset) / ShowGrid.GridRowCount - ShowGrid.BlockOffset;
            ShowGrid.InitializeGrid(pictureBox1);
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    GridBlock cell = new GridBlock();
                    cell.BlockRowIndex = i;
                    cell.BlockColumnIndex = j;
                    cell.BlockText = (DateTime.Now.Millisecond % 9 + 1).ToStringEx();    // (i * 10 + j).ToStringEx(); // //
                    cell.BlockIndex = i * 9 + j;
                    cell.BorderColor = Color.White;
                    cells.Add(cell);
                    if (!(new int[] { 3, 4, 5 }).Contains(i) && !(new int[] { 3, 4, 5 }).Contains(j))
                    {
                        cell.BlockColor = Color.Gray;
                        cell.BorderColor = Color.Gray;
                    }
                    if (((new int[] { 3, 4, 5 }).Contains(i) && (new int[] { 3, 4, 5 }).Contains(j)))
                    {
                        cell.BlockColor = Color.Gray;
                        cell.BorderColor = Color.Gray;
                    }
                    ShowGrid.PaintBlock(cell);

                }
            }

            foreach (GridBlock cell in cells)
            {
                CheckResult(cell, true);
            }


        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            GridBlock cell = cells.Find(c => c.BlockLocation.X < e.X && c.BlockLocation.Y < e.Y
         && c.BlockLocation.X + c.BlockSize.Width > e.X && c.BlockLocation.Y + c.BlockSize.Height > e.Y);
            if (cell==null) return;
            if (string.IsNullOrEmpty(cell.BlockText))
            {
                cell.BlockText = "0";
            }
            if (e.Button.Equals(MouseButtons.Left))
            {

                cell.BlockText = (cell.BlockText.ToIntEx() + 1).ToStringEx();
                if (cell.BlockText.ToIntEx() >= 10) cell.BlockText = "1";
            }
            else
            {
                cell.BlockText = (cell.BlockText.ToIntEx() - 1).ToStringEx();
                if (cell.BlockText.ToIntEx() <= 0) cell.BlockText = "9";
            }



            cell.BlockColor = Color.Green;
            ShowGrid.PaintBlock(cell);
            //foreach (GridBlock c in cells)
            //{
            CheckResult(cell, false);
            //}
        }

        private List<GridBlock> CheckResult(GridBlock gridBlock, bool empty)
        {
            List<GridBlock> chk9 = new List<GridBlock>();
            List<GridBlock> chkr = new List<GridBlock>();
            List<GridBlock> chkc = new List<GridBlock>();
            List<GridBlock> chka = new List<GridBlock>();
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    chk9.Clear();
                    chkr.Clear();
                    chkc.Clear();
                    chka.Clear();
                    for (int k = 0; k < 3; k++)
                    {
                        for (int l = 0; l < 3; l++)
                        {
                            GridBlock chkcell = cells.Find(c => c.BlockColumnIndex.Equals(i * 3 + k) && c.BlockRowIndex.Equals(j * 3 + l));
                            if (chkcell != null)
                                chk9.Add(chkcell);
                        }
                    }
                    if (chk9.Find(c => c.BlockRowIndex.Equals(gridBlock.BlockRowIndex) && c.BlockColumnIndex.Equals(gridBlock.BlockColumnIndex)) != null)
                    {
                        chkr.AddRange(cells.Where(c => c.BlockRowIndex.Equals(gridBlock.BlockRowIndex) && c.BlockText != string.Empty).ToList());
                        chkc.AddRange(cells.Where(c => c.BlockColumnIndex.Equals(gridBlock.BlockColumnIndex) && c.BlockText != string.Empty).ToList());
                        chka.AddRange(chk9);
                        chka.AddRange(chkr);
                        chka.AddRange(chkc);
                        foreach (GridBlock chkcell in chka)
                        {
                            if (chkcell.BlockColor != Color.Green && chkcell.BlockText != string.Empty & !empty)
                            {
                                chkcell.BlockColor = Color.Green;
                                ShowGrid.PaintBlock(chkcell);
                            }
                        }
                        EachResult(chk9, empty);
                        EachResult(chkr, empty);
                        EachResult(chkc, empty);
                        return chka;
                    }
                }
            }
            return chka;
        }

        private void EachResult(List<GridBlock> chk, bool empty)
        {
            foreach (IGrouping<string, GridBlock> grp in chk.GroupBy(c => c.BlockText))
            {
                List<GridBlock> gbs = grp.ToList<GridBlock>();
                if (gbs.Count > 1)
                {
                    foreach (GridBlock gb in gbs)
                    {
                        if (gb.BlockText != string.Empty)
                        {
                            if (empty)
                            {
                                gb.BlockText = "";
                                ShowGrid.PaintBlock(gb);
                            }
                            else
                            {
                                gb.BlockColor = Color.DeepPink;
                                ShowGrid.PaintBlock(gb);
                            }
                        }
                    }
                }
            }
        }
    }
}