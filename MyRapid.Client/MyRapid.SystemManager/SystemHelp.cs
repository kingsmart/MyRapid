/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
using MyRapid.Base.Page;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace MyRapid.SystemManager
{
    public partial class SystemHelp : ChildPage
    {
        public SystemHelp()
        {
            InitializeComponent();
        }
        private void gdf_DataSourceChanged(object sender, EventArgs e)
        {
            trl.DataSource = gdl.DataSource;
        }
        private void gcl_DoubleClick(object sender, EventArgs e)
        {
            trl.Visible = !trl.Visible;
        }
    }
}