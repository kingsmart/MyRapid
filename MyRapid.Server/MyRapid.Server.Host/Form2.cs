/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
using MyRapid.Code;
using MyRapid.Server.Service;
using System;
using System.Configuration;
using System.Drawing;
using System.ServiceModel;
using System.Windows.Forms;

namespace MyRapid.Server.Host
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }
        ServiceHost host;
        private void button1_Click(object sender, EventArgs e)
        {            
            host = new ServiceHost(typeof(MainService));
            try
            {
                if (host.State != CommunicationState.Opened)
                {
                    host.Opened += delegate
                    {
                        label1.ForeColor = Color.Green;
                        linkLabel1.Text = host.BaseAddresses[0].AbsoluteUri;
                        if (radioButton1.Checked)
                        {
                            SqlHelper.connectionString = ConfigurationManager.AppSettings["TestString"];
                        }
                        else
                        {
                            SqlHelper.connectionString = ConfigurationManager.AppSettings["ConnString"];
                        }
                    };
                    host.Open();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (host.State == CommunicationState.Opened)
                {
                    host.Closed += delegate
                    {
                        label1.ForeColor = Color.Red;
                    };
                    host.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}