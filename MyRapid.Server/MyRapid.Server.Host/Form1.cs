/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
using MyRapid.Code;
using System;
using System.Configuration;
using System.Drawing;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Windows.Forms;

namespace MyRapid.Server.Host
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        ServiceHost host;
        private void button1_Click(object sender, EventArgs e)
        {

            host = new ServiceHost(typeof(Service.MainService), new Uri("http://127.0.0.1:4824/MainService"));
            //1.创建ServiceHost对象 这里我们添加了服务的地址，如果这里没有添加服务地址，那么我们在添加服务终结点的时候就需要添加完整路径
            //2.添加服务终结点 注意后面的地址我设置为空 因为我在前面添加了地址
            host.AddServiceEndpoint(typeof(Service.IMainService),   new WSHttpBinding(), "");
            //添加验证Behavior
            host.Description.Behaviors.Add( new AuthenticationBehavior());
            if (host.Description.Behaviors.Find<ServiceMetadataBehavior>() == null)
            {
                //3.添加元数据终结点 上面添加了这个判断的原因 就是查找配置文件中是否配置了元数据行为 因为我们重命名了配置文件 所以元数据行为肯定为null
                ServiceMetadataBehavior metaDataBehavior = new ServiceMetadataBehavior();//创建元数据行为对象
                metaDataBehavior.HttpGetEnabled = true;//重点 启用元数据行为的访问
                metaDataBehavior.HttpGetUrl = new Uri("http://127.0.0.1:4824/MainService/msdl");
                host.Description.Behaviors.Add(metaDataBehavior);//添加元数据行为
            }
            //添加打开服务宿主的执行函数 本例为匿名函数
            host.Opened += delegate
            {
                label1.ForeColor = Color.Green;
                linkLabel1.Text = host.BaseAddresses[0].AbsoluteUri;
                if (radioButton1.Checked)
                {
                    SqlHelper.connectionString = ConfigurationManager.AppSettings["TestString"];
                }
                else
                {
                    SqlHelper.connectionString = ConfigurationManager.AppSettings["ConnString"];
                }
            };
            try
            {
                if (host.State != CommunicationState.Opened)
                {
                    host.Open();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {            
            host.Closed += delegate
            {
                label1.ForeColor = Color.Red;
            };
            try
            {
                if (host.State == CommunicationState.Opened)
                {
                    host.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}