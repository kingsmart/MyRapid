/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
using System;
using System.ComponentModel;
namespace MyRapid.SysEntity
{
    ///<summary>
    ///表Sys_Process的实体类
    ///</summary>
    public class Sys_Process
    {
        [Browsable(false)]
        public string Process_Id { get; set; }
        [Category("Designer")]
        public string Process_Name { get; set; }
        [Category("Designer")]
        public string Process_Nick { get; set; }
        [Category("Extension")]
        public bool IsEnabled { get; set; }
        [Browsable(false)]
        public bool IsDelete { get; set; }
        [Category("Extension")]
        public string Remark { get; set; }
        [Browsable(false)]
        public string Create_User { get; set; }
        [Browsable(false)]
        public System.DateTime Create_Time { get; set; }
        [Browsable(false)]
        public string Modify_User { get; set; }
        [Browsable(false)]
        public System.DateTime Modify_Time { get; set; }
        [Browsable(false)]
        public string Delete_User { get; set; }
        [Browsable(false)]
        public System.DateTime Delete_Time { get; set; }
    }
}