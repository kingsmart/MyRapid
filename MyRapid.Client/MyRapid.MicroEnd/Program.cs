﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyRapid.Launcher
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Form frm = (Form)LoadForm(ConfigurationManager.AppSettings["StartupPath"] + "MyRapid.Client.exe", "MyRapid.Client.MainUI");
            Application.Run(frm);
        }

        static object LoadForm(string asmFile, string asmClass)
        {
            try
            {
                //if (!File.Exists(asmFile)) return null;
                //读取为Byte[]防止出现文件占用
                //byte[] fileData = System.IO.File.ReadAllBytes(asmFile);
                //加载程序集（EXE 或 DLL）
                System.Reflection.Assembly assembly = System.Reflection.Assembly.LoadFrom(asmFile);
                //创建类的实例
                object form = assembly.CreateInstance(asmClass);
                return form;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    throw ex.InnerException;
                }
                throw ex;
            }
        }
    }
}
