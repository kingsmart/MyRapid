/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * First Create: 2012/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
namespace MyRapid.SystemManager
{
    partial class SystemFilter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.gcl = new DevExpress.XtraEditors.GroupControl();
            this.gdl = new DevExpress.XtraGrid.GridControl();
            this.gvl = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcf = new DevExpress.XtraEditors.GroupControl();
            this.fWhere = new DevExpress.XtraEditors.TextEdit();
            this.fSql = new DevExpress.XtraEditors.FilterControl();
            this.spl = new DevExpress.XtraEditors.SplitContainerControl();
            this.qry = new DevExpress.XtraEditors.GroupControl();
            ((System.ComponentModel.ISupportInitialize)(this.gcl)).BeginInit();
            this.gcl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gdl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcf)).BeginInit();
            this.gcf.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fWhere.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spl)).BeginInit();
            this.spl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.qry)).BeginInit();
            this.SuspendLayout();
            // 
            // splitterControl1
            // 
            this.splitterControl1.Location = new System.Drawing.Point(280, 31);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(5, 392);
            this.splitterControl1.TabIndex = 4;
            this.splitterControl1.TabStop = false;
            // 
            // gcl
            // 
            this.gcl.Controls.Add(this.gdl);
            this.gcl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcl.Location = new System.Drawing.Point(0, 0);
            this.gcl.Name = "gcl";
            this.gcl.Size = new System.Drawing.Size(280, 307);
            this.gcl.TabIndex = 5;
            this.gcl.Text = "方案";
            // 
            // gdl
            // 
            this.gdl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gdl.Location = new System.Drawing.Point(2, 21);
            this.gdl.MainView = this.gvl;
            this.gdl.Name = "gdl";
            this.gdl.Size = new System.Drawing.Size(276, 284);
            this.gdl.TabIndex = 0;
            this.gdl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvl});
            // 
            // gvl
            // 
            this.gvl.GridControl = this.gdl;
            this.gvl.Name = "gvl";
            this.gvl.OptionsSelection.MultiSelect = true;
            this.gvl.OptionsView.ColumnAutoWidth = false;
            this.gvl.OptionsView.ShowFooter = true;
            this.gvl.OptionsView.ShowGroupPanel = false;
            // 
            // gcf
            // 
            this.gcf.Controls.Add(this.fWhere);
            this.gcf.Controls.Add(this.fSql);
            this.gcf.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcf.Location = new System.Drawing.Point(285, 31);
            this.gcf.Name = "gcf";
            this.gcf.Size = new System.Drawing.Size(377, 392);
            this.gcf.TabIndex = 8;
            this.gcf.Text = "条件";
            // 
            // fWhere
            // 
            this.fWhere.Location = new System.Drawing.Point(295, 366);
            this.fWhere.Name = "fWhere";
            this.fWhere.Size = new System.Drawing.Size(100, 20);
            this.fWhere.TabIndex = 1;
            // 
            // fSql
            // 
            this.fSql.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.fSql.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.fSql.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fSql.Location = new System.Drawing.Point(2, 21);
            this.fSql.Name = "fSql";
            this.fSql.Size = new System.Drawing.Size(373, 369);
            this.fSql.TabIndex = 0;
            this.fSql.Text = "fSql";
            this.fSql.FilterChanged += new DevExpress.XtraEditors.FilterChangedEventHandler(this.fSql_FilterChanged);
            // 
            // spl
            // 
            this.spl.Dock = System.Windows.Forms.DockStyle.Left;
            this.spl.Horizontal = false;
            this.spl.IsSplitterFixed = true;
            this.spl.Location = new System.Drawing.Point(0, 31);
            this.spl.Name = "spl";
            this.spl.Panel1.Controls.Add(this.qry);
            this.spl.Panel1.Text = "Panel1";
            this.spl.Panel2.Controls.Add(this.gcl);
            this.spl.Panel2.Text = "Panel2";
            this.spl.Size = new System.Drawing.Size(280, 392);
            this.spl.SplitterPosition = 80;
            this.spl.TabIndex = 2;
            this.spl.Text = "splitContainerControl1";
            // 
            // qry
            // 
            this.qry.Dock = System.Windows.Forms.DockStyle.Fill;
            this.qry.Location = new System.Drawing.Point(0, 0);
            this.qry.Name = "qry";
            this.qry.Size = new System.Drawing.Size(280, 80);
            this.qry.TabIndex = 0;
            // 
            // SystemFilter
            // 
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(662, 450);
            this.Controls.Add(this.gcf);
            this.Controls.Add(this.splitterControl1);
            this.Controls.Add(this.spl);
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "SystemFilter";
            this.QueryControl = this.qry;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Shown += new System.EventHandler(this.SystemFilter_Shown);
            this.Controls.SetChildIndex(this.spl, 0);
            this.Controls.SetChildIndex(this.splitterControl1, 0);
            this.Controls.SetChildIndex(this.gcf, 0);
            ((System.ComponentModel.ISupportInitialize)(this.gcl)).EndInit();
            this.gcl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gdl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcf)).EndInit();
            this.gcf.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fWhere.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spl)).EndInit();
            this.spl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.qry)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }
        #endregion
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private DevExpress.XtraEditors.GroupControl gcl;
        private DevExpress.XtraGrid.GridControl gdl;
        private DevExpress.XtraGrid.Views.Grid.GridView gvl;
        private DevExpress.XtraEditors.GroupControl gcf;
        private DevExpress.XtraEditors.FilterControl fSql;
        private DevExpress.XtraEditors.TextEdit fWhere;
        private DevExpress.XtraEditors.SplitContainerControl spl;
        private DevExpress.XtraEditors.GroupControl qry;
    }
}